<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssembliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assemblies', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->date('date');
            $table->enum('status', ['pending', 'open', 'closed']);
            $table->text('announce');
        });

        Schema::create('assembly_user', function (Blueprint $table) {
            $table->integer('assembly_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('delegate_id')->unsigned()->default(0);
            $table->primary(['assembly_id', 'user_id']);

            $table->foreign('assembly_id')->references('id')->on('assemblies')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assembly_user');
        Schema::dropIfExists('assemblies');
    }
}
