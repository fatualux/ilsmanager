<?php

if (function_exists('render_account_hierarchy') == false) {
    function render_account_hierarchy($accounts, $deep, $select, $hide_archived)
    {
        $ret = '';

        foreach($accounts as $account) {
            if($hide_archived == true && $account->archived) {
                continue;
            }

            $ret .= '<option value="' . $account->id . '" ' . ($select == $account->id ? 'selected' : '') . '>' . join('', array_fill(0, $deep, '-')) . ' ' . $account->name . '</option>';
            $ret .= render_account_hierarchy($account->children, $deep + 2, $select, $hide_archived);
        }

        return $ret;
    }
}

if (!isset($name)) {
    $name = 'account[]';
}

if (!isset($select)) {
    $select = 0;
}

if (!isset($hide_archived)) {
    $hide_archived = true;
}

?>

<select class="form-control" name="{{ $name }}" autocomplete="off">
    <option value="0" {{ $select == 0 ? 'selected' : '' }}>Nessuno</option>
    {!! render_account_hierarchy(App\Account::whereNull('parent_id')->orderBy('name', 'asc')->with('children')->get(), 0, $select, $hide_archived) !!}
</select>
