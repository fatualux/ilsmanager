@extends('layouts.app')

@section('content')
<div class="container">
    @can('create', App\Assembly::class)
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createAssembly">Registra Nuova</button>
                <div class="modal fade" id="createAssembly" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Convoca Assemblea</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ route('assembly.store') }}">
                                @csrf
                                <div class="modal-body">
                                    @include('assembly.form', ['object' => null])
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary">Salva</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row mt-3">
        <div class="col-md-12">
            <div class="accordion" id="assemblies_list">
                @foreach($objects as $assembly)
                    <div class="card">
                        <div class="card-header" id="head_assembly_{{ $assembly->id }}">
                            <h2 class="mb-0">
                                <button class="btn btn-link {{ $assembly->status != 'open' ? 'collapsed' : '' }}" type="button" data-toggle="collapse" data-target="#assembly_{{ $assembly->id }}">
                                    {{ printableDate($assembly->date) }}
                                </button>
                            </h2>
                        </div>
                        <div id="assembly_{{ $assembly->id }}" class="collapse {{ $assembly->status == 'open' ? 'show' : '' }}" data-parent="#assemblies_list">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        @include('assembly.votations', ['assembly' => $assembly])

                                        @can('update', $assembly)
                                            <form method="POST" action="{{ route('assembly.update', $assembly->id) }}">
                                                @method('PUT')
                                                @csrf

                                                @include('assembly.form', ['object' => $assembly])

                                                <div class="form-group row">
                                                    <div class="col-sm-10">
                                                        <button type="submit" class="btn btn-primary">Salva</button>
                                                    </div>
                                                </div>
                                            </form>

                                            <hr>

                                            @include('assembly.managevotations', ['assembly' => $assembly])
                                        @else
                                            <p>
                                                {!! nl2br($assembly->announce) !!}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <?php $actually_partecipating = $assembly->partecipating() ?>

                                        @if($assembly->status == 'open')
                                            <div class="row mb-3">
                                                <div class="col-md-12">
                                                    @if($actually_partecipating)
                                                        <form method="POST" action="{{ route('assembly.partecipate', ['assembly_id' => $assembly->id, 'do' => 0]) }}">
                                                            @csrf
                                                            <button type="submit" class="btn btn-primary">Revoca Partecipazione</button>
                                                        </form>
                                                    @else
                                                        <form method="POST" action="{{ route('assembly.partecipate', ['assembly_id' => $assembly->id, 'do' => 1]) }}">
                                                            @csrf
                                                            <button type="submit" class="btn btn-primary">Segnala Partecipazione</button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="list-group">
                                                        @foreach($assembly->users as $partecipant)
                                                            <li class="list-group-item">
                                                                {{ $partecipant->printable_name }}

                                                                @if($actually_partecipating == false && $partecipant->pivot->delegate_id == 0 && $assembly->users()->wherePivot('delegate_id', $partecipant->id)->count() < 3)
                                                                    <form method="POST" action="{{ route('assembly.delegate', ['assembly_id' => $assembly->id, 'delegate_id' => $partecipant->id]) }}" class="float-right">
                                                                        @csrf
                                                                        <button type="submit" class="btn">Delega</button>
                                                                    </form>
                                                                @elseif($partecipant->pivot->delegate_id != 0)
                                                                    {{ $partecipant->pivot->delegate_id != 0 ? '(ha delegato ' . App\User::find($partecipant->pivot->delegate_id)->printable_name . ')' : '' }}
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        @else
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="list-group">
                                                        @foreach($assembly->users as $partecipant)
                                                            <li class="list-group-item">{{ $partecipant->printable_name }} {{ $partecipant->pivot->delegate_id != 0 ? '(ha delegato ' . App\User::find($partecipant->pivot->delegate_id)->printable_name . ')' : '' }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
