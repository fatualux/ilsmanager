@extends('layouts.app')

@section('content')
<div class="container">
    @can('create', App\Volunteer::class)
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createVolunteer">Registra Nuovo</button>
                <div class="modal fade" id="createVolunteer" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Registra Volontario</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ route('volunteer.store') }}">
                                @csrf
                                <div class="modal-body">
                                    @include('volunteer.form', ['object' => null])
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary">Salva</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row mt-3">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Cognome</th>
                        <th>Nome</th>
                        <th>E-Mail</th>
                        <th>Sezione</th>

                        @if($currentuser->hasRole('admin'))
                            <th>Modifica</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($objects as $volunteer)
                        <tr>
                            <td>{{ $volunteer->name }}</td>
                            <td>{{ $volunteer->surname }}</td>
                            <td>{{ $volunteer->email }}</td>
                            <td>{{ $volunteer->section ? $volunteer->section->city : '' }}</td>

                            @if($currentuser->hasRole('admin'))
                                <td>
                                    <a href="{{ route('volunteer.edit', $volunteer->id) }}"><span class="oi oi-pencil"></span></a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
