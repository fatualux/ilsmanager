@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if($currentuser->hasRole('admin'))
                <form method="POST" action="{{ route('section.update', $object->id) }}">
                    @method('PUT')
                    @csrf

                    @include('section.form', ['object' => $object])

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Salva</button>
                        </div>
                    </div>
                </form>
            @endif

            <div class="card mt-3">
                <div class="card-header">Contabilità</div>

                <div class="card-body">
                    <div class="alert alert-info">
                        Disponibilità Attuale: {{ $object->balance }} €
                    </div>

                    @if($object->economic_logs()->count() != 0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Data</th>
                                    <th>Ammontare</th>
                                    <th>Causale</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($object->economic_logs()->orderBy('created_at', 'desc')->take(10)->get() as $log)
                                    <tr>
                                        <td>{{ $log->created_at }}</td>
                                        <td>{{ $log->amount }}</td>
                                        <td>{{ $log->reason }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <a class="btn btn-secondary" href="{{ route('section.balance', $object->id) }}">Consulta lo storico integrale</a>
                    @endif
                </div>
            </div>

            <div class="card mt-3">
                <div class="card-header">Soci</div>

                <div class="card-body">
                    @if($object->users->count() == 0)
                        <div class="alert alert-danger">
                            Non ci sono soci affiliati a questa sezione locale.
                        </div>
                    @else
                        <form method="POST" action="{{ route('section.update', $object->id) }}">
                            @method('PUT')
                            @csrf

                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Cognome</th>
                                        <th>Ruolo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($object->users as $user)
                                        <tr>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->surname }}</td>
                                            <td>
                                                @can('update', $object)
                                                    <input type="hidden" name="user[]" value="{{ $user->id }}">
                                                    <select class="form-control" name="role[]">
                                                        <option value="referent" {{ $user->hasRole('referent') ? 'selected' : '' }}>Referente</option>
                                                        <option value="member" {{ $user->hasRole('referent') == false ? 'selected' : '' }}>Affiliato</option>
                                                    </select>
                                                @else
                                                    @if($user->hasRole('referent'))
                                                        Referente
                                                    @else
                                                        Affiliato
                                                    @endif
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            @can('update', $object)
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Salva</button>
                                    </div>
                                </div>
                            @endcan

                        </form>

                        @can('update', $object)
                        <form class="mt-3" method="POST" action="{{ route('section.exportmembers', $object->id) }}">
                         @csrf
                         <div class="row">
                             <div class="col-sm-12">
                                <button type="submit" class="btn btn-secondary">Esporta Contatti (CSV)</button>
                            </div>
                        </div>
                        </form>
                        @endcan
                    @endif
                </div>
            </div>

            <div class="card mt-3">
                <div class="card-header">Volontari</div>

                <div class="card-body">
                    @can('update', $object)
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    Qui puoi aggiungere e rimuovere persone che, pur non essendo soci, partecipano all'attività della tua sezione locale. È importante mantenere aggiornata questa lista nel tempo, in quanto i volontari qui iscritti sono soggetti alla copertura assicurativa.
                                </p>

                                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#createVolunteer">Registra Volontario</button>
                                <div class="modal fade" id="createVolunteer" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Registra Volontario</h5>
                                                <button type="button" class="close" data-dismiss="modal">
                                                    <span>&times;</span>
                                                </button>
                                            </div>
                                            <form method="POST" action="{{ route('volunteer.store') }}">
                                                @csrf
                                                <div class="modal-body">
                                                    @include('volunteer.form', ['object' => null, 'section_id' => $object->id])
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                                    <button type="submit" class="btn btn-primary">Salva</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>
                    @endif

                    @if($object->volunteers->count() == 0)
                        <div class="alert alert-danger">
                            Non ci sono volontari in questa sezione locale.
                        </div>
                    @else
                        <form method="POST" action="{{ route('section.update', $object->id) }}">
                            @method('PUT')
                            @csrf

                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Cognome</th>
                                        <th>Nome</th>
                                        <th>Email</th>

                                        @can('update', $object)
                                            <th>Elimina</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($object->volunteers as $volunteer)
                                        <tr>
                                            @can('update', $object)
                                                <td>
                                                    <input type="hidden" name="volunteer[]" value="{{ $volunteer->id }}">
                                                    <input type="text" class="form-control" name="name[]" value="{{ $volunteer->name }}">
                                                </td>
                                                <td><input type="text" class="form-control" name="surname[]" value="{{ $volunteer->surname }}"></td>
                                                <td><input type="text" class="form-control" name="email[]" value="{{ $volunteer->email }}"></td>
                                                <td><input type="checkbox" name="remove[]" value="{{ $volunteer->id }}"></td>
                                            @else
                                                <td>{{ $volunteer->name }}</td>
                                                <td>{{ $volunteer->surname }}</td>
                                                <td>{{ $volunteer->email }}</td>
                                            @endcan
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            @can('update', $object)
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Salva</button>
                                    </div>
                                </div>
                            @endcan
                        </form>
                    @endif
                </div>
            </div>

            @can('delete', $object)
                <form class="mt-3" method="POST" action="{{ route('section.destroy', $object->id) }}">
                    @method('DELETE')
                    @csrf

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-danger">Elimina Sezione Locale</button>
                        </div>
                    </div>
                </form>
            @endcan
        </div>
    </div>
</div>
@endsection
