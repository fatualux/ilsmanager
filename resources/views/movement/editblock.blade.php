<ul class="list-group">
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-3">
                {{ $movement->date }}<br />
		<small>{{ $movement->bank->type }}</small>
            </div>
            <div class="col-md-3">
                {{ $movement->amount }}
            </div>
            <div class="col-md-6">
                {{ $movement->notes }}
            </div>
        </div>
    </li>

    @if($movement->account_rows->isEmpty() == false)
        @foreach($movement->account_rows as $ar)
            @include('accountrow.editblock', ['ar' => $ar])
        @endforeach
    @elseif(($guessed = $movement->guessRows())->isEmpty() == false)
        @foreach($guessed as $ar)
            @include('accountrow.editblock', ['ar' => $ar])
        @endforeach
    @else
        @include('accountrow.editblock', ['ar' => null])
    @endif

    <li class="list-group-item">
        <div class="form-check float-right">
            <input class="form-check-input" type="checkbox" name="remove[]" value="{{ $movement->id }}" id="remove{{ $movement->id }}">
            <label class="form-check-label" for="remove{{ $movement->id }}">
                Elimina
            </label>
        </div>
        <button class="btn btn-default btn-sm add-row"><span class="oi oi-plus"></span></button>
        <ul class="hidden">
            @include('accountrow.editblock', ['ar' => null])
        </ul>
    </li>
</ul>
