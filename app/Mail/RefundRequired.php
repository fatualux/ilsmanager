<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RefundRequired extends Mailable
{
    use Queueable, SerializesModels;

    private $refund = null;

    public function __construct($refund)
    {
        $this->refund = $refund;
    }

    public function build()
    {
        return $this->subject('Richiesta rimborso spese')->view('email.new_refund', ['refund' => $this->refund]);
    }
}
