<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VotationController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => 'App\Votation',
            'view_folder' => 'votation'
        ]);
    }

    protected function defaultValidations($object)
    {
        return [
            'assembly_id' => 'required',
            'question' => 'required',
            'status' => 'required|max:255',
            'max_options' => 'required',
        ];
    }

    protected function requestToObject($request, $object)
    {
        if ($request->user()->hasRole('admin')) {
            $object->assembly_id = $request->input('assembly_id');
            $object->question = $request->input('question');
            $object->status = $request->input('status');
            $object->max_options = $request->input('max_options');
            $object->options = array_filter($request->input('option', []));
        }

        return $object;
    }

    public function index()
    {
        return redirect()->route('assembly.index');
    }

    public function edit($id)
    {
        return redirect()->route('assembly.index');
    }
}
