<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VolunteerController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => 'App\Volunteer',
            'view_folder' => 'volunteer'
        ]);
    }

    protected function requestToObject($request, $object)
    {
        $object->name = $request->input('name');
        $object->surname = $request->input('surname');
        $object->email = $request->input('email');
        $object->section_id = $request->user()->hasRole('admin') ? $request->input('section_id', 0) : $request->user()->section_id;
        return $object;
    }

    protected function defaultValidations($object)
    {
        $ret = [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'email' => 'required|email|max:255',
        ];

        return $ret;
    }

    protected function defaultSortingColumn()
    {
        return 'surname';
    }
}
