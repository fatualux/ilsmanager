<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\EconomicLog;

class Section extends Model
{
    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function volunteers()
    {
        return $this->hasMany('App\Volunteer');
    }

    public function economic_logs()
    {
        return $this->hasMany('App\EconomicLog');
    }

    public function alterBalance($amount, $account_row, $reason)
    {
        $this->balance += $amount;
        $this->save();

        $log = new EconomicLog();
        $log->section_id = $this->id;
        $log->amount = $amount;
        $log->account_row_id = $account_row ? $account_row->id : 0;
        $log->reason = $reason;
        $log->save();
    }
}
