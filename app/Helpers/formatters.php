<?php

function mailPasswordHash($s)
{
    $salt = \Illuminate\Support\Str::random(10);
    return crypt($s, $salt);
}

function printableDate($value)
{
    if (is_null($value)) {
        return _i('Mai');
    }
    else {
        $t = strtotime($value);
        return ucwords(strftime('%A %d %B %G', $t));
    }
}
